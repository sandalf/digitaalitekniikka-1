LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

entity counterbcd is
	port(
		enable : in std_logic;
		clock : in std_logic;
		button : in std_logic;
		updown : in std_logic;
		reset : in std_logic;
		mux : in std_logic_vector(1 downto 0);
		bcd : out std_logic_vector(18 downto 0)
	);
end counterbcd;

architecture RTL of counterbcd is
	signal count1 : unsigned(3 downto 0) := "0000";
	signal count2 : unsigned(3 downto 0) := "0110";
	signal count3 : unsigned(3 downto 0) := "0000";
	signal output : unsigned(18 downto 0) := to_unsigned(60, 19);
	
	begin
	process(clock, button, updown, mux) is
		begin
		if rising_edge(clock) then
			if reset = '0' then
				count1 <= "0000";
				count2 <= "0110";
				count3 <= "0000";
			elsif button = '1' and enable = '1' then
				if mux = "00" then
					if updown = '1' then
						if count1 = "0010" then
							count1 <= "0000";
						else
							count1 <= count1 + 1;
						end if;
					else
						if count1 = "0000" then
							count1 <= "0010";
						else
							count1 <= count1 - 1;
						end if;
					end if;
				elsif mux = "01" then
					if updown = '1' then
						if count2 = "1001" then
							count2 <= "0000";
						else
							count2 <= count2 + 1;
						end if;
					else
						if count2 = "0000" then
							count2 <= "1001";
						else
							count2 <= count2 - 1;
						end if;
					end if;
				elsif mux = "10" then
					if updown = '1' then
						if count3 = "1001" then
							count3 <= "0000";
						else
							count3 <= count3 + 1;
						end if;
					else
						if count3 = "0000" then
							count3 <= "1001";
						else
							count3 <= count3 - 1;
						end if;
					end if;
				end if;
			end if;
			bcd <= std_logic_vector(("0000000" & count1 & count2 & count3));
		end if;
	end process;
end architecture RTL;
			
			