LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

entity unsigned_divide is
	generic(
		DATA_WIDTH_NUMERATOR : natural := 16;
		DATA_WIDTH_DENOMINATOR : natural := 16
	);
	
	port(
		numerator : in unsigned((DATA_WIDTH_NUMERATOR - 1) downto 0);
		denominator : in unsigned((DATA_WIDTH_DENOMINATOR - 1) downto 0);
		quatient : out unsigned((DATA_WIDTH_NUMERATOR -1) downto 0)
	);
end unsigned_divide;

architecture RTL of unsigned_divide is
	begin
	quatient <= numerator / denominator;
end RTL;