LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

entity freq_generator is
	port(
		clock : in std_logic;
		count_limit : in unsigned(31 downto 0);
		reset : in std_logic;
		counter_binary : out std_logic_vector(31 downto 0);
		BIST : out std_logic
	);
end freq_generator;

architecture RTL of freq_generator is
	signal count : unsigned(31 downto 0);
	signal limit : unsigned(31 downto 0) := to_unsigned(to_integer(count_limit), 32); -- pakollinen spaghetti että toimii
	
	begin
	process(clock) is
		begin
		if reset = '0' then
			count <= (others => '0');
		elsif rising_edge(clock) then
			if count < limit then
				count <= count + 1;
			else
				count <= (others => '0');
			end if;
		end if;
	end process;
	
	process(count) is
		begin
		if count = limit then
			BIST <= '1';
		else
			BIST <= '0';
		end if;
		counter_binary <= std_logic_vector(count);
	end process;
end architecture RTL;
	