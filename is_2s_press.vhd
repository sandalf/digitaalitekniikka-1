LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

entity is_2s_press is
	port(
		button : in std_logic;
		clock : in std_logic;
		long : out std_logic := '0';
		short : out std_logic := '0'
	);
end is_2s_press;

architecture RTL of is_2s_press is
	signal count : unsigned(31 downto 0);
	signal over_two : std_logic := '0';
	signal under_two : std_logic := '0';
	
	begin
	process(clock, button) is
		begin
		if rising_edge(clock) then
			if button = '0' then 
				count <= count + 1;
				over_two <= '0';
				under_two <= '0';
			else
				if count > to_unsigned(100000000, 32) then
					count <= (others => '0');
					over_two <= '1';
					under_two <= '0';
				elsif count = to_unsigned(0, 32) then
					under_two <= '0';
					over_two <= '0';
				else
					count <= (others => '0');
					under_two <= '1';
					over_two <= '0';
				end if;
			end if;
			short <= under_two;
			long <= over_two;
		end if;
	end process;
end architecture RTL;