LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

entity numberswitch is
	port(
		button : in std_logic;
		clock : in std_logic;
		reset : in std_logic;
		mux : out std_logic_vector(1 downto 0)
	);
end numberswitch;

architecture RTL of numberswitch is
	signal mux_mode : unsigned(1 downto 0) := "00";
	
	begin
	process(clock, button) is
		begin
		if rising_edge(clock)then
			if reset = '0' then
				mux_mode <= "00";
			elsif button = '1' then
				if mux_mode = "11" then
					mux_mode <= "00";
				elsif mux_mode = "00" then
					mux_mode <= "01";
				elsif mux_mode = "01" then
					mux_mode <= "10";
				elsif mux_mode = "10" then
					mux_mode <= "00";
				end if;
			end if;
		mux <= std_logic_vector(mux_mode);
		end if;
	end process;
end architecture RTL;