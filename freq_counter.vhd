LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

entity freq_counter is
	port(
		clock : in std_logic;
		input_frequency : in std_logic;
		reset : in std_logic;
		counter_binary : out std_logic_vector(31 downto 0);
		cout : out std_logic
	);
end freq_counter;

architecture RTL of freq_counter is
	signal count : unsigned(31 downto 0);
	signal co : std_logic;
	
	begin
	process(clock, input_frequency) is
		begin
		if reset = '0' then
			count <= (others => '0');
		elsif rising_edge(clock) then
			if input_frequency = '1' then
				count <= (others => '0');
				co <= '1';
			else
				count <= count + 1;
				co <= '0';
			end if;
		end if;
	end process;
	counter_binary <= std_logic_vector(count);
	cout <= co;
end architecture RTL;