LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

entity control_state_machine is
	port(
		long : in std_logic;
		short : in std_logic;
		clock : in std_logic;
		reset : in std_logic;
		long_mux : out std_logic;
		short_mux : out std_logic;
		short_out : out std_logic
	);
end control_state_machine;

architecture RTL of control_state_machine is
	signal mux_mode_long : std_logic := '0';
	signal mux_mode_short : std_logic := '1';
	signal under_two : std_logic := '0';
	
	begin
	process(long) is
		begin
			if rising_edge(clock) then
				if reset = '0' then
					mux_mode_long <= '0';
					mux_mode_short <= '1';
					under_two <= '0';
				elsif long = '1' then
					if mux_mode_long = '1' then
						mux_mode_long <= '0';
						mux_mode_short <= '0';
					else
						mux_mode_long <= '1';
					end if;
				elsif short = '1' then
					mux_mode_short <= not mux_mode_short;
					under_two <= '1';
				else
					under_two <= '0';
				end if;
			end if;
			long_mux <= mux_mode_long;
			short_mux <= mux_mode_short;
			short_out <= under_two;
	end process;
end architecture RTL;