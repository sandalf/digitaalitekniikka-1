LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

entity compare32bit is
	port(
		data : in unsigned(31 downto 0);
		clock : in std_logic;
		reset : in std_logic;
		frequency_high : out std_logic_vector(31 downto 0);
		frequency_low : out std_logic_vector(31 downto 0)
	);
end compare32bit;

architecture RTL of compare32bit is
	signal storage_high : unsigned(31 downto 0) := to_unsigned(0, 32);
	signal storage_low : unsigned(31 downto 0) := to_unsigned(10000, 32);
	
	begin
	process(data) is
		begin
		if rising_edge(clock) then
			if reset = '0' then
				storage_high <= to_unsigned(0, 32);
				storage_low <= to_unsigned(10000, 32);
			elsif data /= (data'range => '0') then
				if data > storage_high then
					storage_high <= data;
				end if;
				if data < storage_low then
					storage_low <= data;
				end if;
			end if;
		end if;
		frequency_high <= std_logic_vector(storage_high);
		frequency_low <= std_logic_vector(storage_low);
	end process;
end RTL;