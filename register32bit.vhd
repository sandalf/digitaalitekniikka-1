LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

entity register32bit is
	port(
		clock : in std_logic;
		load : in std_logic;
		data : in unsigned(31 downto 0);
		reset : in std_logic;
		output : out std_logic_vector(31 downto 0)
	);
end register32bit;

architecture RTL of register32bit is
	signal storage : unsigned(31 downto 0) := (others => '0');
	
	begin
	process(clock, load, data, reset) is
		begin
		if reset = '0' then
			storage <= (others => '0');
		elsif rising_edge(clock) and load = '1' then
			storage <= data;
		end if;
		output <= std_logic_vector(storage);
	end process;
end RTL;