LIBRARY ieee;
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all;

entity mode_control is
	port(
		button : in std_logic;
		clock : in std_logic;
		in_out_mux : out std_logic := '1'
	);
end mode_control;

architecture RTL of mode_control is
	signal mux_mode : std_logic := '1';
	
	begin
	process(clock) is
		begin
		if rising_edge(clock) then
			if button = '0' then
				mux_mode <= not mux_mode;
			end if;
		end if;
	end process;
	in_out_mux <= mux_mode;
end architecture RTL;